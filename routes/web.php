<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@getIndexPage')->name('products.get');
Route::get('/products/create', 'IndexController@getCreateProductPage')->name('products_create.get');
Route::post('/products', 'ProductController@saveProduct')->name('product.save');
Route::post('/types', 'ProductTypeController@saveProductType')->name('product_type.save');
