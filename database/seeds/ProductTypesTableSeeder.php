<?php

use App\Models\Attributes;
use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(ProductType::class, 3)->create()->each(function ($productType){
            factory(Product::class, 1)->create([
                'product_type_id' => $productType->id
            ])
                ->each(function ($product){
                factory(Attributes::class, 3)->create(['product_id' => $product->id]);
            });
        });
    }
}
