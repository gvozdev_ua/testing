<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker, $attrib) {
    return [
        'title' => $faker->sentence(random_int(2, 5)),
        'sku' => $faker->word,
        'product_type_id' => $attrib['product_type_id']
    ];
});
