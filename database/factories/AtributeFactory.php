<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Attributes;
use Faker\Generator as Faker;

$factory->define(Attributes::class, function (Faker $faker, $attrib) {
    return [
        'product_id' => $attrib['product_id'],
        'name' => $faker->word,
        'value' => $faker->word,
    ];
});
