$(document).ready(function () {


    $(".repeater-add-btn").on('click', function (e){
        e.preventDefault();
        e.stopPropagation();

        return false;
    });

    $("#repeater").createRepeater();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#save_product_type").on('click', function () {
        let type_name = $("input#product_type").val();
        $.ajax({
            type: 'post',
            url: '/types',
            data: {
                name: type_name
            },
            success: function (response) {
                $("#create-product-type-modal").modal('hide');
                $('.modal-backdrop').remove();

                let option = "<option value=" + response.id + ">" + response.name + "</option>";
                $("#product_type").append(option);
            }
        });

    });
});