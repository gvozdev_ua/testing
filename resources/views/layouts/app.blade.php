<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')
</head>
<body>
    <div class="d-flex" id="wrapper">

        @include('partials.sidebar')


        <!-- Page Content -->
            <div id="page-content-wrapper">

            @yield('content')

            </div>
            <!-- /#page-content-wrapper -->
    </div>
</body>
</html>
