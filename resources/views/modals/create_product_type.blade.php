<div class="modal fade text-left" id="create-product-type-modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="CategoryLabel">Add New Product Type</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="create-category-form" method="POST" class="form form-horizontal" action="{{route('product_type.save')}}">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group row">
                            <div class="col">
                                <label class="label-control" for="product_type">Name</label>
                                <input type="text" id="product_type" class="form-control @error('name') is-invalid @enderror" name="product_type">
                                <span class="invalid-feedback"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
                        Close
                    </button>

                    <button type="button" id="save_product_type" class="btn btn-outline-primary">
                        Save Type
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>