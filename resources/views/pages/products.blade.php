@extends('layouts.app')
@section('content')
<div class="container-fluid">

    <h1 class="mt-4">Products</h1>
    <table class="table table-striped mb-5">
        <thead>
        <tr>
            <th>product id</th>
            <th>sku</th>
            <th>title</th>
            <th>type</th>
            <th>attributes</th>
        </tr>
        </thead>
        <tbody>
        @if(count($products) > 0)
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id}}</td>
                    <td>{{$product->sku}}</td>
                    <td>{{$product->title}}</td>
                    <td>{{$product->productType->name}}</td>
                    <td>
                        @foreach($product->attributes as $value)
                            <span class="badge badge-info">{{$value['name']}} @if ($value) : @endif {{$value['value']}}</span>
                        @endforeach
                    </td>
                </tr>
            @endforeach
                @else
                    <p>You have no products</p>
                @endif
                </tbody>
            </table>
            <div class="row">
                {{ $products->links() }}
            </div>
</div>
 @stop

