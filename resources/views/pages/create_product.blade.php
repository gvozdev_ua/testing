@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Create new Product</h1>
                <form role="form" method="POST"
                      action="{{route('product.save')}}"
                >
                    @csrf
                    <div class="form-group">
                        <label for="sku">SKU:</label>
                        <input name="sku" id="sku" type="text" class="form-control @error('sku') is-invalid @enderror"
                               value="{{ old('sku') }}">

                        @error('sku')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input id="title" name="title" type="text" class="form-control @error('title') is-invalid @enderror"
                               value="{{old('title')}}">

                        @error('title')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="row align-items-end">
                            <div class="col-8">
                                <label for="product_type">Select Type:</label>
                                <select id="product_type" name="product_type_id" class="form-control @error('type_id') is-invalid @enderror">
                                    <option value="" selected disabled>Select type</option>
                                    @foreach($types as $type)
                                        <option @if(old('type') == $type->id) selected
                                                @endif value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>

                                @error('type')
                                <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                            </div>

                            <div class="col-4">
                                <button class="btn btn-success" data-toggle="modal"
                                        data-target="#create-product-type-modal" type="button" href="#">Add Product Type
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="repeater">
                        <!-- Repeater Heading -->
                        <div class="repeater-heading">
                            <button class="btn btn-primary repeater-add-btn">
                                Add attribute
                            </button>

                        </div>
                        <div class="items" data-group="attributes">
                            <div class="row form-group mt-3">
                                <div class="col-4">
                                    <input class="form-control" placeholder="Attribute name" data-skip-name="false" data-name="name"
                                           value="{{old('attribute')}}">

                                    @error('attribute')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="col-4">
                                    <input type="text" class="form-control" id="inputEmail" placeholder="Attribute value" data-skip-name="false" data-name="value">
                                    @error('value')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="col-4">
                                    <!-- Repeater Remove Btn -->
                                    <div class="pull-right repeater-remove-btn">
                                        <button id="remove-btn" class="btn btn-danger" onclick="$(this).parents('.items').remove()">
                                            Remove
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </form>
                <!-- Create Type Modal -->
                @include('modals.create_product_type')
    </div>
@stop
