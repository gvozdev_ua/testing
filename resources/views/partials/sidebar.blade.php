<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="list-group list-group-flush">

        <ul class="list-group">
            <li class="list-group-item {{ (request()->route()->getName() === 'products.get') ? 'active' : '' }}">
                <a href="{{route('products.get')}}"
                    class=" list-group-item-action " >
                    Products</a>
            </li>
            <li class="list-group-item {{ (request()->route()->getName() === 'products_create.get') ? 'active' : '' }}">
                <a href="{{route('products_create.get')}}"
                    class="list-group-item-action ">
                    Create product</a></li>
        </ul>
    </div>
</div>
<!-- /#sidebar-wrapper -->
