<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Models\Product;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function saveProduct(CreateProductRequest $request, Product $product)
    {
        DB::transaction(function () use ($product, $request){
            $product->fill($request->only('sku', 'title', 'product_type_id'));

            if($product->save()){
                $attributes = $request->get('attributes');

                if ($attributes) {
                    $product->attributes()->createMany($attributes);
                }
            }
        });

        return redirect()->route('products.get');
    }
}
