<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function getCreateProductPage()
    {
        $types = ProductType::all();

        return view('pages.create_product', compact('types'));
    }

    public function getIndexPage()
    {
        $products = Product::paginate(Product::PER_PAGE);
        $types = ProductType::all();

        return view('pages.products', compact('products', 'types'));
    }
}
