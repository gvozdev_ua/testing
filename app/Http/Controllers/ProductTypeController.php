<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductTypeRequest;
use App\Models\ProductType;

class ProductTypeController extends Controller
{

    public function saveProductType(CreateProductTypeRequest $request)
    {
        return ProductType::create($request->only(['name']));
    }
}
