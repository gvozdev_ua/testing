<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => 'min:3|max:50|string|required',
            'title' => 'min:3|max:50|string|required',
            'product_type_id' => 'exists:product_types,id|string|required',
            'attributes' => 'nullable|array',
        ];
    }
}
