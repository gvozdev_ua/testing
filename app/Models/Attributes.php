<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    protected $fillable = [
        'name',
        'value',
        'product_id'
    ];

    public function products()
    {
        return $this->belongsTo(Product::class);
    }
}
