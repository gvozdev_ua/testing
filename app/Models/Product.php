<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public const PER_PAGE = 10;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'sku', 'product_type_id'
    ];

    public function attributes()
    {
        return $this->hasMany(Attributes::class, 'product_id', 'id' );
    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class);
    }
}
